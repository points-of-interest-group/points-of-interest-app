import {ACCESS_TOKEN, API_BASE_URL, API_ADMIN_URL, GOOGLE_PLACES_API_URL, CORS_PROXY} from "../constants";
import {stringify} from "query-string";

const request = (options) => {
    const headers = new Headers({
        'Content-Type': 'application/json'
    });

    if (localStorage.getItem(ACCESS_TOKEN)) {
        headers.append('Authorization', 'Bearer ' + localStorage.getItem(ACCESS_TOKEN))
    }

    const defaults = {headers: headers};
    options = Object.assign({}, defaults, options);

    return fetch(options.url, options)
        .then(response =>
            response.text().then(text => {
                const json = text ? JSON.parse(text) : {};
                if (response.status < 200 || response.status >= 300) {
                    return Promise.reject(json);
                }

                return json;
            })
        );
};

export function loadAllMarkers() {
    return request( {
        url: API_ADMIN_URL + '/poi',
        method: 'GET',
    })
}

export function getMarkers(filter) {
    return request( {
        url: API_BASE_URL + '/poi?' + stringify(filter),
        method: 'GET',
    })
}

export function getMarkerById(id) {
    return request({
        url: API_BASE_URL + '/poi/' + id,
        method: 'GET'
    })
}

export function loadMarkersWithin(filter) {
    return request( {
        url: API_BASE_URL + '/poi/within?' + stringify(filter),
        method: 'GET'
    })
}

export function addMarkerInfo(feature) {
    return request({
        url: API_ADMIN_URL + '/poi',
        method: 'POST',
        body: JSON.stringify(feature)
    })
}

export function addMarkerRequest(feature) {
    return request({
        url: API_BASE_URL + '/poi',
        method: 'POST',
        body: JSON.stringify(feature)
    })
}

export function updateMarkerInfo(feature) {
    return request({
        url: API_ADMIN_URL + '/poi',
        method: 'PUT',
        body: JSON.stringify(feature)
    })
}

export function updateRequest(feature) {
    return request({
        url: API_BASE_URL + '/poi',
        method: 'PUT',
        body: JSON.stringify(feature)
    })
}

export function changeStatus(changeStatusRequest) {
    return request({
        url: API_ADMIN_URL + '/poi',
        method: 'PATCH',
        body: JSON.stringify(changeStatusRequest)
    })
}

export function deletePoint(id) {
    return request({
        url: API_ADMIN_URL + '/poi/' + id,
        method: 'DELETE'
    })
}

export function ratePointOfInterest(body) {
    return request({
        url: API_BASE_URL + '/rate',
        method: 'POST',
        body: JSON.stringify(body)
    })
}

export function getUserList() {
    return request({
        url: API_ADMIN_URL + '/user',
        method: 'GET'
    })
}

export function getUserById(userId) {
    return request({
        url: API_ADMIN_URL + '/user/' + userId,
        method: 'GET'
    })
}

export function updateUser(userId, userData) {
    return request({
        url: API_ADMIN_URL + '/user/' + userId,
        method: 'PUT',
        body: JSON.stringify(userData)
    })
}

export function deleteUser(userId) {
    return request({
        url: API_ADMIN_URL + '/user/' + userId,
        method: 'DELETE'
    })
}

export function login(loginRequest) {
    return request({
        url: API_BASE_URL + '/auth/signin',
        method: 'POST',
        body: JSON.stringify(loginRequest)
    })
}

export function signup(signupRequest) {
    return request({
        url: API_BASE_URL + "/auth/signup",
        method: 'POST',
        body: JSON.stringify(signupRequest)
    })
}

export function getCurrentUser() {
    if (!localStorage.getItem(ACCESS_TOKEN)) {
        return Promise.reject("No access token set.")
    }

    return request({
        url: API_BASE_URL + "/user/me",
        method: 'GET'
    })
}

export function performSearch(searchValue) {
    return request({
        url: CORS_PROXY + GOOGLE_PLACES_API_URL + '&input=' + searchValue,
        method: 'GET'
    })
}