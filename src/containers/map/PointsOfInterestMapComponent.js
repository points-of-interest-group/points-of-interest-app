import React, {Component} from "react";
import {geolocated} from "react-geolocated";
import {loadMarkersWithin} from "../../util/APIUtils";
import {GoogleMap, InfoWindow, withGoogleMap, withScriptjs} from "react-google-maps";
import * as _ from "lodash";
import mapStyles from "./mapStyles";
import {compose, withProps} from "recompose";
import {GOOGLE_MAP_API_URL} from "../../constants";
import './PointsOfInterestMapComponent.css';
import PointsOfInterestMarker from "../../../src/components/map/PointsOfInterestMarker";
import {Button} from "react-bootstrap";
import {CurrentUserContext} from "../../util/CurrentUserContext";

class PointsOfInterestMapComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            map: null,
            markers: [],
            markersDrawn: [],
            isLoading: false,
            waitForStop: false,
            windowPosition: null,
            infoWindowOpen: false,
            coordsToAdd: null
        };

        this.onCenterChanged = this.onCenterChanged.bind(this);
        this.debouncedOnCenterChanged = _.debounce(this.debouncedOnCenterChanged.bind(this), 500);
        this.toggleInfoWindow = this.toggleInfoWindow.bind(this);
    }

    static contextType = CurrentUserContext;

    componentDidMount() {
        this.loadMarkers();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        if (prevProps.isAuthenticated !== this.props.isAuthenticated) {
            this.setState({
                markers: [],
                isLoading: false,
            });
            this.loadMarkers();
        }
    }

    arrayUnique = (array) => {
        let a = array.concat();
        for (let i = 0; i < a.length; ++i) {
            for (let j = i + 1; j < a.length; ++j) {
                if (a[i].properties.id === a[j].properties.id)
                    a.splice(j--, 1);
            }
        }

        return a;
    };

    onMapMounted = (map) => {
        this.setState({
            map: map
        })
    };

    onCenterChanged = () => {
        this.debouncedOnCenterChanged();
    };

    toggleInfoWindow = (loc) => {
        if (loc == null) {
            this.setState({infoWindowOpen: false});
            return;
        }

        let markerLocation = {lat: loc.latLng.lat(), lng: loc.latLng.lng()};
        this.setState({
            windowPosition: markerLocation,
            infoWindowOpen: true
        });
    };

    onYesButtonClick = () => {
        const poi = {
            latitude: this.state.windowPosition.lat,
            longitude: this.state.windowPosition.lng,
            name: ''
        };

        this.props.handleCreatePOI(poi);
    };

    debouncedOnCenterChanged = () => {
        this.updateMarkers();
    };

    updateMarkers = () => {
        const center = this.state.map.getCenter();
        this.loadMarkers({latitude: center.lat(), longitude: center.lng()});
    };

    loadMarkers = (coords = {latitude: 56.1397684, longitude: 40.3839377}, radius = 0.05) => {
        const filter = {
            geometry: JSON.stringify({
                type: 'Point',
                coordinates: [coords.latitude, coords.longitude]
            }),
            distance: radius
        };
        const params = {
            filter: JSON.stringify(filter)
        };
        let promise = loadMarkersWithin(params);
        if (!promise) {
            return;
        }

        this.setState({
            isLoading: true
        });

        promise
            .then(response => {
                const markers = this.state.markers.slice();
                const result = this.arrayUnique(markers.concat(response.features));
                this.setState({
                    markers: result,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    };

    render() {
        if (!this.props.isGeolocationAvailable) return (
            <div>Your browser does not support Geolocation</div>
        );
        if (!this.props.isGeolocationEnabled) return (
            <div>Geolocation is not enabled</div>
        );
        if (!this.props.coords) return (
            <div>Getting the location data&hellip; </div>
        );

        const markerViews = [];
        this.state.markers.forEach((feature, markerIndex) => {
            markerViews.push(
                <PointsOfInterestMarker
                    featureId={feature.properties.id}
                    key={feature.properties.id}
                    featureIndex={markerIndex}/>
            )
        });

        let currentUser = this.context;
        let isUser = currentUser !== null && currentUser.role === 'ROLE_USER';

        return (
            <GoogleMap
                defaultZoom={14}
                defaultCenter={{
                    lat: this.props.coords.latitude,
                    lng: this.props.coords.longitude
                }}
                defaultOptions={{
                    styles: mapStyles,
                    streetViewControl: false,
                    scaleControl: false,
                    mapTypeControl: false,
                    panControl: false,
                    zoomControl: false,
                    rotateControl: false,
                    fullscreenControl: false
                }}
                ref={this.onMapMounted}
                onCenterChanged={this.onCenterChanged}
                onClick={this.toggleInfoWindow}>
                {markerViews}
                {!isUser && this.state.infoWindowOpen &&
                    <InfoWindow
                        position={this.state.windowPosition}
                        onCloseClick={this.toggleInfoWindow}>
                        <div className="InfoWindow">
                            <div className="modal-body">
                                <p>Add new point here?</p>
                            </div>
                            <div className="modal-footer">
                                <Button className="btn-primary" onClick={this.onYesButtonClick}>Yes</Button>
                                <Button onClick={() => this.toggleInfoWindow(null)}>No</Button>
                            </div>
                        </div>
                    </InfoWindow>
                }
            </GoogleMap>
        );
    };
}

export default compose(
    withProps({
        googleMapURL: GOOGLE_MAP_API_URL,
        loadingElement: <div style={{height: `100%`}}/>,
        containerElement: <div style={{height: `400px`}}/>,
        mapElement: <div style={{height: `100%`}}/>,
    }),
    geolocated({
        positionOptions: {
            enableHighAccuracy: false,
        },
        userDecisionTimeout: 5000
    }),
    withScriptjs,
    withGoogleMap
)(PointsOfInterestMapComponent);