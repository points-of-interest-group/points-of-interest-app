import React, {Component} from "react";
import {Form, FormGroup, FormControl, ControlLabel, Radio} from "react-bootstrap";
import "./Signup.css";
import LoaderButton from "../../../components/buttons/LoaderButton";
import {signup} from "../../../util/APIUtils";

export default class Signup extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            role: "ROLE_USER",
            name: "",
            username: "",
            password: "",
            confirmPassword: "",
            newUser: null
        };
    }

    validateForm() {
        return (
            this.state.name.length > 0 &&
            this.state.username.length > 0 &&
            this.state.password.length > 0 &&
            this.state.password === this.state.confirmPassword
        );
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleRoleChange = event => {
        this.setState({
            role: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        const signupRequest = {
            name: this.state.name,
            username: this.state.username,
            password: this.state.password,
            role: this.state.role
        };

        await signup(signupRequest)
            .then(response => {
                if (response.success) {
                    this.props.history.push("/login");
                } else {
                    alert(response.message);
                }
            })
            .catch(error => {
                alert(error.message);
                this.setState({isLoading: false});
            })
    };

    render() {
        return (
            <div className="Signup">
                <Form onSubmit={this.handleSubmit}>
                    <FormGroup bsSize="large">
                        <ControlLabel>Role</ControlLabel>
                        <FormGroup>
                            <Radio inline defaultChecked name="roleRadioGroup" value="ROLE_USER" onChange={this.handleRoleChange}>
                                Customer
                            </Radio>{' '}
                            <Radio inline name="roleRadioGroup" value="ROLE_MANAGER" onChange={this.handleRoleChange}>
                                Manager
                            </Radio>
                        </FormGroup>
                    </FormGroup>
                    <FormGroup controlId="name" bsSize="large">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl autoFocus type="input" value={this.state.name} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl autoFocus type="input" value={this.state.username} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl value={this.state.password} onChange={this.handleChange} type="password"/>
                    </FormGroup>
                    <FormGroup controlId="confirmPassword" bsSize="large">
                        <ControlLabel>Confirm Password</ControlLabel>
                        <FormControl value={this.state.confirmPassword} onChange={this.handleChange} type="password"/>
                    </FormGroup>
                    <LoaderButton block bsSize="large" disabled={!this.validateForm()} type="submit"
                                  isLoading={this.state.isLoading} text="Signup" loadingText="Signing up…"/>
                </Form>
            </div>
        );
    }
}