import React, { Component } from "react";
import { FormGroup, FormControl, ControlLabel } from "react-bootstrap";
import "./Login.css";
import {login} from "../../../util/APIUtils";
import {ACCESS_TOKEN} from "../../../constants";
import LoaderButton from "../../../components/buttons/LoaderButton";

export default class Login extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: false,
            username: "",
            password: ""
        };
    }

    componentDidMount() {
        if (!this.props.isAuthenticated) {
            this.props.history.push('/');
        }
    }

    validateForm() {
        return this.state.username.length > 0 && this.state.password.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        this.setState({isLoading: true});

        const loginRequest = {
            "username" : this.state.username,
            "password" : this.state.password
        };

        await login(loginRequest)
            .then(response => {
                localStorage.setItem(ACCESS_TOKEN, response.accessToken);
                this.props.handleLogin(true);
            })
            .catch(error => {
                if (error.status === 401) {
                    alert("Incorrect username or password.");
                } else {
                    alert(error.message);
                }

                this.setState({isLoading: false});
        })
    };

    render() {
        return (
            <div className="Login">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl autoFocus type="input" value={this.state.username} onChange={this.handleChange} />
                    </FormGroup>
                    <FormGroup controlId="password" bsSize="large">
                        <ControlLabel>Password</ControlLabel>
                        <FormControl value={this.state.password} onChange={this.handleChange} type="password" />
                    </FormGroup>
                    <LoaderButton block bsSize="large" disabled={!this.validateForm()}
                                  type="submit" isLoading={this.state.isLoading}
                                  text="Login" loadingText="Logging in…" />
                </form>
            </div>
        );
    }
}