import React, {Component} from "react";
import {getUserById, updateUser} from "../../../util/APIUtils";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import LoaderButton from "../../../components/buttons/LoaderButton";
import './UserEdit.css';

export default class UserEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            username: '',
            role: '',
            isLoading: false
        }
    }

    componentDidMount() {
        this.loadUser(this.props.location.state.id);
    }

    loadUser(id) {
        this.setState({
            isLoading: true
        });
        getUserById(id)
            .then(response => {
                console.log(response);
                this.setState({
                    name: response.name,
                    username: response.username,
                    role: response.role,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    validateForm() {
        return this.state.name.length > 0 &&
            this.state.username.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        let user = {
            name: this.state.name,
            username: this.state.username,
            role: this.state.role
        };

        this.setState({
            isLoading: true
        });

        updateUser(this.props.location.state.id, user)
            .then(() => {
                this.setState({
                    isLoading: false
                });

                const location = {
                    pathname: '/admin/user/detail',
                    state: {
                        id: this.props.location.state.id
                    }
                };

                this.props.history.push(location);
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        return (!(this.state.latitude === '') &&
            <div className="User">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="name" bsSize="large">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl type="text" value={this.state.name} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="username" bsSize="large">
                        <ControlLabel>Username</ControlLabel>
                        <FormControl type="text" value={this.state.username} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="role" bsSize="large">
                        <ControlLabel>Status</ControlLabel>
                        <FormControl componentClass="select" placeholder="select" value={this.state.role} onChange={this.handleChange}>
                            <option value="ROLE_MANAGER">Manager</option>
                            <option value="ROLE_USER">User</option>
                        </FormControl>
                    </FormGroup>
                    <LoaderButton block bsSize="large" disabled={!this.validateForm()}
                                  type="submit" isLoading={this.state.isLoading}
                                  text="Edit user" loadingText="Inserting…"/>
                </form>
            </div>
        );
    }
}