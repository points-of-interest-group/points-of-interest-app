import React, {Component} from "react";
import {getUserList} from "../../../util/APIUtils";
import {PageHeader} from "react-bootstrap";
import BootstrapTable from "react-bootstrap-table-next";
import {withRouter} from "react-router-dom";

class UserList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            users: [],
            isLoading: true
        };

        this.redirectToDetail = this.redirectToDetail.bind(this);
    }

    componentDidMount() {
        getUserList()
            .then(response => {
                this.setState({
                    users: response,
                    isLoading: false
                })
            })
    }

    redirectToDetail(e, user) {
        e.preventDefault();

        const location = {
            pathname: '/admin/user/detail',
            state: {
                id: user.id
            }
        };

        this.props.history.push(location);
    }

    roleFormatter = (role) => {
        if (role === 'ROLE_ADMIN') {
            return 'Administrator';
        }
        if (role === 'ROLE_USER') {
            return "User";
        }
        if (role === 'ROLE_MANAGER') {
            return "Manager";
        }
    };

    render() {
        const columns = [
            {
                dataField: 'id',
                text: 'ID',
                editable: false
            },
            {
                dataField: 'username',
                text: 'Username',
                editable: false
            },
            {
                dataField: 'name',
                text: 'Name',
                editable: false
            },
            {
                dataField: 'role',
                text: 'Role',
                editable: false,
                formatter: this.roleFormatter
            }
        ];

        const rowEvents = {
            onClick: this.redirectToDetail
        };

        return (
            <div className="poiList">
                <PageHeader>Users.</PageHeader>
                {!this.state.isLoading &&
                <BootstrapTable
                    keyField="id"
                    data={this.state.users}
                    columns={columns}
                    rowEvents={rowEvents}
                    condensednoDataIndication="Table is Empty"
                    striped
                    hover
                />
                }
            </div>
        )
    }
}

export default withRouter(UserList);