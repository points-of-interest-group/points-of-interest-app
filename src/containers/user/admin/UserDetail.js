import React, {Component} from "react";
import {Button, ButtonGroup, ButtonToolbar, Col, Grid, Modal, PageHeader, Row} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {deleteUser, getUserById} from "../../../util/APIUtils";
import './UserDetail.css';

class UserDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            user: null,
            isLoading: true,
            showModal: false
        };

        this.redirectToEdit = this.redirectToEdit.bind(this);
        this.deleteUser = this.deleteUser.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        this.loadUser(this.props.location.state.id)
    }

    handleShow() {
        this.setState({
            showModal: true
        })
    }

    handleClose() {
        this.setState({
            showModal: false
        })
    }

    redirectToEdit() {
        const location = {
            pathname: '/admin/user/edit',
            state: {
                id: this.state.user.id
            }
        };

        this.props.history.push(location);
    }

    loadUser(id) {
        getUserById(id)
            .then(response => {
                this.setState({
                    user: response,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    deleteUser() {
        deleteUser(this.state.user.id)
            .then(() => {
                this.props.history.push('/admin/user/list');
            })
            .catch(error => {
                console.log(error);
            })
    }

    formatRole(role) {
        if (role === 'ROLE_ADMIN') {
            return 'Administrator';
        }
        if (role === 'ROLE_USER') {
            return "User";
        }
        if (role === 'ROLE_MANAGER') {
            return "Manager";
        }
    }

    render() {
        if (this.state.isLoading) return null;
        const user = this.state.user;
        const isAdmin = user.role === 'ROLE_ADMIN';
        return (
            <div>
                <Modal bsSize="small" show={this.state.showModal} onHide={this.handleClose}>
                    <Modal.Header>
                        <Modal.Title>Delete user</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>Do you want to delete {user.name}?</Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                        <Button onClick={this.deleteUser} bsStyle="primary">Delete</Button>
                    </Modal.Footer>
                </Modal>
                <Link style={{float: 'right'}} to="/admin/user/list">Back</Link>
                <PageHeader>{user.name}</PageHeader>
                <div style={{maxWidth: '25%'}}>
                    <Grid fluid className="Detail">
                        <Row>
                            <Col xs={4}>ID:</Col>
                            <Col xs={8}>{user.id}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Name:</Col>
                            <Col xs={8}>{user.name}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Username:</Col>
                            <Col xs={8}>{user.username}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Role:</Col>
                            <Col xs={8}>{this.formatRole(user.role)}</Col>
                        </Row>
                    </Grid>
                    {!isAdmin &&
                    <ButtonToolbar style={{marginTop: '20px'}}>
                        <ButtonGroup bsSize="large">
                            <Button onClick={this.redirectToEdit}>Edit</Button>
                            <Button onClick={this.handleShow} bsStyle="danger">Delete</Button>
                        </ButtonGroup>
                    </ButtonToolbar>
                    }
                </div>
            </div>
        );
    }
}

export default withRouter(UserDetail);