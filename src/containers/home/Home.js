import React, {Component} from "react";
import "./Home.css";
import PointsOfInterestMapComponent from "../map/PointsOfInterestMapComponent";
import AutocompleteSearch from "../../components/map/AutocompleteSearch";
import {CurrentUserContext} from "../../util/CurrentUserContext";

export default class Home extends Component {
    static contextType = CurrentUserContext;

    render() {
        let currentUser = this.context;
        let isAdmin = currentUser !== null && currentUser.role === 'ROLE_ADMIN';

        return (
            <div className="Home">
                <div className="lander">
                    <h1>Map</h1>
                </div>
                {isAdmin && <AutocompleteSearch handleCreatePOI={this.props.handleCreatePOI}/>}
                <div>
                    <PointsOfInterestMapComponent handleCreatePOI={this.props.handleCreatePOI}/>
                </div>
            </div>
        );
    }
}