import React, {Component} from "react";
import {PageHeader} from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import {withRouter} from "react-router-dom";

class PlaceList extends Component {
    constructor(props) {
        super(props);

        this.redirectToDetails = this.redirectToDetails.bind(this);
    }

    redirectToDetails(e, feature) {
        e.preventDefault();

        const location = {
            pathname: '/place/detail',
            state: {
                id: feature.properties.id
            }
        };

        this.props.history.push(location);
    }

    render() {
        const rowEvents = {
            onClick: this.redirectToDetails
        };

        return (
            <div className="poiList">
                <PageHeader>{this.props.title}</PageHeader>
                    <BootstrapTable
                        keyField="id"
                        data={this.props.values}
                        columns={this.props.columns}
                        rowEvents={ rowEvents }
                        condensednoDataIndication="Table is Empty"
                        striped
                        hover
                    />
            </div>
        )
    }
}

export default withRouter(PlaceList);