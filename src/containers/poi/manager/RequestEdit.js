import React, {Component} from "react";
import {getMarkerById, updateRequest} from "../../../util/APIUtils";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import LoaderButton from "../../../components/buttons/LoaderButton";
import './RequestEdit.css';

export default class RequestEdit extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: '',
            longitude: '',
            name: '',
            description: '',
            status: '',
            isLoading: false
        }
    }

    componentDidMount() {
        this.loadPointOfView(this.props.location.state.id);
    }

    loadPointOfView(id) {
        this.setState({
            isLoading: true
        });
        getMarkerById(id)
            .then(response => {
                console.log(response);
                this.setState({
                    latitude: response.geometry.coordinates[0],
                    longitude: response.geometry.coordinates[1],
                    name: response.properties.name,
                    description: response.properties.description,
                    status: response.properties.status,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    validateForm() {
        return this.state.name.length > 0 &&
            this.state.description.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        let feature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [this.state.latitude, this.state.longitude]
            },
            properties: {
                id: this.props.location.state.id,
                name: this.state.name,
                description: this.state.description,
            }
        };

        this.setState({
            isLoading: true
        });

        updateRequest(feature)
            .then(() => {
                this.setState({
                    isLoading: false
                });

                const location = {
                    pathname: '/place/detail',
                    state: {
                        id: this.props.location.state.id
                    }
                };

                this.props.history.push(location);
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        return (!(this.state.latitude === '') &&
            <div className="POI">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="latitude" bsSize="large">
                        <ControlLabel>Latitude</ControlLabel>
                        <FormControl readOnly type="text" value={this.state.latitude} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="longitude" bsSize="large">
                        <ControlLabel>Longitude</ControlLabel>
                        <FormControl readOnly type="text" value={this.state.longitude} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="name" bsSize="large">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl autoFocus type="text" value={this.state.name} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="description" bsSize="large">
                        <ControlLabel>Description</ControlLabel>
                        <FormControl type="text" value={this.state.description} onChange={this.handleChange}/>
                    </FormGroup>
                    <LoaderButton block bsSize="large" disabled={!this.validateForm()}
                                  type="submit" isLoading={this.state.isLoading}
                                  text="Edit point" loadingText="Inserting…"/>
                </form>
            </div>
        );
    }
}