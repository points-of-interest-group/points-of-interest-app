import React, {Component} from "react";
import {Button, ButtonGroup, ButtonToolbar, Col, Grid, PageHeader, Row} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {getMarkerById} from "../../../util/APIUtils";
import './PlaceDetail.css';

class PlaceDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            feature: null,
            isLoading: true
        };

        this.redirectToEdit = this.redirectToEdit.bind(this);
    }

    componentDidMount() {
        this.loadPointOfView(this.props.location.state.id)
    }

    redirectToEdit() {
        const location = {
            pathname: '/request/edit',
            state: {
                id: this.state.feature.properties.id
            }
        };

        this.props.history.push(location);
    }

    loadPointOfView(id) {
        this.setState({
            isLoading: true
        });
        getMarkerById(id)
            .then(response => {
                this.setState({
                    feature: response,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    formatStatus(status) {
        if (status === 0) {
            return ('Wait for approval.')
        }
        if (status === 1) {
            return ('Rejected.')
        }
        if (status === 2) {
            return ('Approved.')
        }
    }


    formatterRate(rate) {
        return rate === '0,00' ? 'Not rated' : rate;
    };

    render() {
        if (this.state.isLoading) return null;
        const feature = this.state.feature;
        const editable = feature.properties.status === 0;
        const approved = feature.properties.status === 2;
        return (
            <div>
                <Link style={{float: 'right'}} to={approved ? "/place/list" : "/request/list"}>Back</Link>
                <PageHeader>{feature.properties.name}</PageHeader>
                <div style={{maxWidth: '25%'}}>
                    <Grid fluid className="Detail">
                        <Row>
                            <Col xs={4}>ID:</Col>
                            <Col xs={8}>{feature.properties.id}</Col>
                        </Row>
                        <Row>
                            <Col xs={12}>Coords:</Col>
                        </Row>
                        <Row>
                            <Col xs={4} style={{paddingLeft: '25px'}}>Lat:</Col>
                            <Col xs={8}>{feature.geometry.coordinates[0]}</Col>
                        </Row>
                        <Row style={{paddingBottom: '10px'}}>
                            <Col xs={4} style={{paddingLeft: '25px'}}>Lng:</Col>
                            <Col xs={8}>{feature.geometry.coordinates[1]}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Name:</Col>
                            <Col xs={8}>{feature.properties.name}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Description:</Col>
                            <Col xs={8}>{feature.properties.description}</Col>
                        </Row>
                        {
                            approved ?
                                <Row>
                                    <Col xs={4}>Rate:</Col>
                                    <Col xs={8}>{this.formatterRate(feature.properties.average)}</Col>
                                </Row>
                                :
                                <Row>
                                    <Col xs={4}>Status:</Col>
                                    <Col xs={8}>{this.formatStatus(feature.properties.status)}</Col>
                                </Row>
                        }
                    </Grid>
                    <ButtonToolbar style={{marginTop: '20px'}}>
                        <ButtonGroup bsSize="large">
                            {editable && <Button onClick={this.redirectToEdit}>Edit</Button>}
                        </ButtonGroup>
                    </ButtonToolbar>
                </div>
            </div>
        );
    }
}

export default withRouter(PlaceDetail);