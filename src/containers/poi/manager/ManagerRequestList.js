import React, {Component} from "react";
import {getMarkers} from "../../../util/APIUtils";
import {withRouter} from "react-router-dom";
import PlaceList from "./PlaceList";

class ManagerRequestList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            poiList: []
        };
    }

    async componentDidMount() {
        if (!this.props.isAuthenticated) {
            return;
        }

        const params = {
            filter: JSON.stringify({
                statuses: [0, 1]
            })
        };

        await getMarkers(params)
            .then(response => {
                this.setState({
                    poiList: response.features,
                    isLoading: false
                })
            })
    }

    geometryFormatter = (geometry) => {
        return (
            'Lat: ' + geometry.coordinates[0] + ', Lng: ' + geometry.coordinates[1]
        )
    };

    statusFormatter = (status) => {
        if (status === 0) {
            return ('Wait for approval.')
        }
        if (status === 1) {
            return ('Rejected.')
        }
        if (status === 2) {
            return ('Approved.')
        }
    };

    render() {
        const columns = [
            {
                dataField: 'id',
                text: 'ID',
                editable: false
            },
            {
                dataField: 'geometry',
                text: 'Coordinates',
                formatter: this.geometryFormatter,
                editable: false
            },
            {
                dataField: 'properties.name',
                text: 'Name'
            },
            {
                dataField: 'properties.description',
                text: 'Description'
            },
            {
                dataField: 'properties.status',
                text: 'Status',
                formatter: this.statusFormatter,
                editable: false
            }
        ];

        return (!this.state.isLoading &&
            <PlaceList
                title="My Requests"
                values={this.state.poiList}
                columns={columns}
            />
        )
    }
}

export default withRouter(ManagerRequestList);