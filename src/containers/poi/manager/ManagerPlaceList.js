import React, {Component} from "react";
import {getMarkers} from "../../../util/APIUtils";
import {withRouter} from "react-router-dom";
import PlaceList from "./PlaceList";

class ManagerPlaceList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            poiList: []
        };
    }

    async componentDidMount() {
        if (!this.props.isAuthenticated) {
            return;
        }

        const params = {
            filter: JSON.stringify({
                statuses: [2]
            })
        };

        await getMarkers(params)
            .then(response => {
                this.setState({
                    poiList: response.features,
                    isLoading: false
                })
            })
    }

    geometryFormatter = (geometry) => {
        return (
            'Lat: ' + geometry.coordinates[0] + ', Lng: ' + geometry.coordinates[1]
        )
    };

    rateFormatter = (rate) => {
        return rate === '0,00' ? 'Not rated' : rate;
    };

    render() {
        const columns = [
            {
                dataField: 'id',
                text: 'ID',
                editable: false
            },
            {
                dataField: 'geometry',
                text: 'Coordinates',
                formatter: this.geometryFormatter,
                editable: false
            },
            {
                dataField: 'properties.name',
                text: 'Name'
            },
            {
                dataField: 'properties.description',
                text: 'Description'
            },
            {
                dataField: 'properties.average',
                text: 'Rating',
                formatter: this.rateFormatter,
                editable: false
            }
        ];

        return (!this.state.isLoading &&
            <PlaceList
                title="My Places"
                values={this.state.poiList}
                columns={columns}
            />
        )
    }
}

export default withRouter(ManagerPlaceList);