import React, {Component} from "react";
import {PageHeader} from "react-bootstrap";
import BootstrapTable from 'react-bootstrap-table-next';
import {loadAllMarkers} from "../../../util/APIUtils";
import {withRouter} from "react-router-dom";

class PointOfInterestList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            isLoading: true,
            poiList: []
        };

        this.redirectToDetails = this.redirectToDetails.bind(this);
    }

    redirectToDetails(e, feature) {
        e.preventDefault();

        const location = {
            pathname: '/admin/poi/detail',
            state: {
                id: feature.properties.id
            }
        };

        this.props.history.push(location);
    }

    geometryFormatter = (geometry) => {
        return (
            'Lat: ' + geometry.coordinates[0] + ', Lng: ' + geometry.coordinates[1]
        )
    };

    statusFormatter = (status) => {
        if (status === 0) {
            return ('Wait for approval.')
        }
        if (status === 1) {
            return ('Rejected.')
        }
        if (status === 2) {
            return ('Approved.')
        }
    };

    rateFormatter = (rate) => {
        return rate === '0,00' ? 'Not rated' : rate;
    };

    async componentDidMount() {
        if (!this.props.isAuthenticated) {
            return;
        }

        await loadAllMarkers()
            .then(response => {
                this.setState({
                    poiList: response.features,
                    isLoading: false
                })
            })
    }

    render() {
        const columns = [
            {
                dataField: 'id',
                text: 'ID',
                editable: false
            },
            {
                dataField: 'geometry',
                text: 'Coordinates',
                formatter: this.geometryFormatter,
                editable: false
            },
            {
                dataField: 'properties.name',
                text: 'Name'
            },
            {
                dataField: 'properties.description',
                text: 'Description'
            },
            {
                dataField: 'properties.status',
                text: 'Status',
                formatter: this.statusFormatter,
                editable: false
            },
            {
                dataField: 'properties.average',
                text: 'Rating',
                formatter: this.rateFormatter,
                editable: false
            }
        ];

        const rowEvents = {
            onClick: this.redirectToDetails
        };

        return (
            <div className="poiList">
                <PageHeader>Points of interests.</PageHeader>
                {!this.state.isLoading &&
                    <BootstrapTable
                        keyField="id"
                        data={this.state.poiList}
                        columns={columns}
                        rowEvents={ rowEvents }
                        condensednoDataIndication="Table is Empty"
                        striped
                        hover
                    />
                }
            </div>
        )
    }
}

export default withRouter(PointOfInterestList);