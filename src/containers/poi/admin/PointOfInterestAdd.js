import React, {Component} from "react";
import {ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import LoaderButton from "../../../components/buttons/LoaderButton";
import "./PointOfInterestAdd.css";
import {addMarkerInfo, addMarkerRequest} from "../../../util/APIUtils";
import {CurrentUserContext} from "../../../util/CurrentUserContext";

export default class PointOfInterestAdd extends Component {
    constructor(props) {
        super(props);

        this.state = {
            latitude: this.props.latitude,
            longitude: this.props.longitude,
            name: this.props.name,
            description: '',
            isLoading: false
        }
    }

    static contextType = CurrentUserContext;

    validateForm() {
        return this.state.name.length > 0 &&
            this.state.description.length > 0;
    }

    handleChange = event => {
        this.setState({
            [event.target.id]: event.target.value
        });
    };

    handleSubmit = async event => {
        event.preventDefault();

        let feature = {
            type: "Feature",
            geometry: {
                type: "Point",
                coordinates: [this.state.latitude, this.state.longitude]
            },
            properties: {
                name: this.state.name,
                description: this.state.description
            }
        };

        this.setState({
            isLoading: true
        });

        let currentUser = this.context;
        let isAdmin = currentUser.role === 'ROLE_ADMIN';
        let isManager = currentUser.role === 'ROLE_MANAGER';

        let response;
        if (isAdmin) {
            response = addMarkerInfo(feature);
        } else if (isManager) {
            response = addMarkerRequest(feature);
        }

        response.then(() => {
                this.setState({
                    isLoading: false
                });

                this.props.history.push("/");
            })
            .catch(err => {
                console.log(err);
            });
    };

    render() {
        if (this.props.latitude === '' || this.props.longitude === '') {
            return (
                <div>Please, return to <a href="/">map</a> and pick the point.</div>
            )
        }

        return (
            <div className="POI">
                <form onSubmit={this.handleSubmit}>
                    <FormGroup controlId="latitude" bsSize="large">
                        <ControlLabel>Latitude</ControlLabel>
                        <FormControl readOnly type="input" value={this.state.latitude} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="longitude" bsSize="large">
                        <ControlLabel>Longitude</ControlLabel>
                        <FormControl readOnly type="input" value={this.state.longitude} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="name" bsSize="large">
                        <ControlLabel>Name</ControlLabel>
                        <FormControl autoFocus type="input" value={this.state.name} onChange={this.handleChange}/>
                    </FormGroup>
                    <FormGroup controlId="description" bsSize="large">
                        <ControlLabel>Description</ControlLabel>
                        <FormControl type="input" value={this.state.description} onChange={this.handleChange}/>
                    </FormGroup>
                    <LoaderButton block bsSize="large" disabled={!this.validateForm()}
                                  type="submit" isLoading={this.state.isLoading}
                                  text="Create point" loadingText="Inserting…"/>
                </form>
            </div>
        );
    }
}