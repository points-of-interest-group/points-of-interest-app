import React, {Component} from "react";
import {Button, ButtonGroup, ButtonToolbar, Col, Grid, Modal, PageHeader, Row} from "react-bootstrap";
import {Link, withRouter} from "react-router-dom";
import {changeStatus, deletePoint, getMarkerById} from "../../../util/APIUtils";
import './PointOfInterestDetail.css';

class PointOfInterestDetail extends Component {
    constructor(props) {
        super(props);

        this.state = {
            feature: null,
            isLoading: true,
            showModal: false
        };

        this.updateStatus = this.updateStatus.bind(this);
        this.redirectToEdit = this.redirectToEdit.bind(this);
        this.deletePoint = this.deletePoint.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
    }

    componentDidMount() {
        this.loadPointOfView(this.props.location.state.id)
    }

    handleShow() {
        this.setState({
            showModal: true
        })
    }

    handleClose() {
        this.setState({
            showModal: false
        })
    }

    redirectToEdit() {
        const location = {
            pathname: '/admin/poi/edit',
            state: {
                id: this.state.feature.properties.id
            }
        };

        this.props.history.push(location);
    }

    loadPointOfView(id) {
        this.setState({
            isLoading: true
        });
        getMarkerById(id)
            .then(response => {
                this.setState({
                    feature: response,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    deletePoint() {
        deletePoint(this.state.feature.properties.id)
            .then(() => {
                this.props.history.push('/admin/poi/list');
            })
            .catch(error => {
                console.log(error);
            })
    }

    updateStatus(statusId) {
        const body = {
            poiId: this.state.feature.properties.id,
            statusId: statusId
        };

        changeStatus(body)
            .then(() => {
                this.props.history.push('/admin/poi/list');
            })
            .catch(error => {
                console.log(error);
            })
    }

    formatStatus(status) {
        if (status === 0) {
            return ('Wait for approval.')
        }
        if (status === 1) {
            return ('Rejected.')
        }
        if (status === 2) {
            return ('Approved.')
        }
    }

    render() {
        let feature = this.state.feature;
        return (!this.state.isLoading &&
            <div>
                <Modal bsSize="small" show={this.state.showModal} onHide={this.handleClose}>
                    <Modal.Header>
                        <Modal.Title>Delete point</Modal.Title>
                    </Modal.Header>

                    <Modal.Body>Do you want to delete {feature.properties.name}?</Modal.Body>

                    <Modal.Footer>
                        <Button onClick={this.handleClose}>Close</Button>
                        <Button onClick={this.deletePoint} bsStyle="primary">Delete</Button>
                    </Modal.Footer>
                </Modal>
                <Link style={{float: 'right'}} to="/admin/poi/list">Back</Link>
                <PageHeader>{feature.properties.name}</PageHeader>
                <div style={{maxWidth: '25%'}}>
                    <Grid fluid className="Detail">
                        <Row>
                            <Col xs={4}>ID:</Col>
                            <Col xs={8}>{feature.properties.id}</Col>
                        </Row>
                        <Row>
                            <Col xs={12}>Coords:</Col>
                        </Row>
                        <Row>
                            <Col xs={4} style={{paddingLeft: '25px'}}>Lat:</Col>
                            <Col xs={8}>{feature.geometry.coordinates[0]}</Col>
                        </Row>
                        <Row style={{paddingBottom: '10px'}}>
                            <Col xs={4} style={{paddingLeft: '25px'}}>Lng:</Col>
                            <Col xs={8}>{feature.geometry.coordinates[1]}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Name:</Col>
                            <Col xs={8}>{feature.properties.name}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Description:</Col>
                            <Col xs={8}>{feature.properties.description}</Col>
                        </Row>
                        <Row>
                            <Col xs={4}>Status:</Col>
                            <Col xs={8}>{this.formatStatus(feature.properties.status)}</Col>
                        </Row>
                    </Grid>
                    <ButtonToolbar style={{marginTop: '20px'}}>
                        {feature.properties.status === 0 &&
                        <ButtonGroup bsSize="large">
                            <Button onClick={() => this.updateStatus(2)} bsStyle="success">Approve</Button>
                            <Button onClick={() => this.updateStatus(1)} bsStyle="warning">Reject</Button>
                        </ButtonGroup>}
                        <ButtonGroup bsSize="large">
                            <Button onClick={this.redirectToEdit}>Edit</Button>
                            <Button onClick={this.handleShow} bsStyle="danger">Delete</Button>
                        </ButtonGroup>
                    </ButtonToolbar>
                </div>
            </div>
        );
    }
}

export default withRouter(PointOfInterestDetail);