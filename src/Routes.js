import React from "react";
import {Route, Switch} from "react-router-dom";
import Home from "./containers/home/Home";
import NotFound from "./containers/exceptions/NotFound";
import Login from "./containers/user/login/Login";
import AppliedRoute from "./components/AppliedRoute";
import Signup from "./containers/user/signup/Signup";
import PoiList from "./containers/poi/admin/PointOfInterestList";
import PointOfInterestAdd from "./containers/poi/admin/PointOfInterestAdd";
import PoiDetail from "./containers/poi/admin/PointOfInterestDetail";
import PointOfInterestEdit from "./containers/poi/admin/PointOfInterestEdit";
import ManagerPointList from "./containers/poi/manager/ManagerPlaceList";
import ManagerRequestList from "./containers/poi/manager/ManagerRequestList";
import PlaceDetail from "./containers/poi/manager/PlaceDetail";
import RequestEdit from "./containers/poi/manager/RequestEdit";
import UserList from "./containers/user/admin/UserList";
import UserDetail from "./containers/user/admin/UserDetail";
import UserEdit from "./containers/user/admin/UserEdit";

export default ({ childProps }) =>
    <Switch>
        <AppliedRoute path="/" exact component={Home} props={childProps} />
        <AppliedRoute path="/login" exact component={Login} props={childProps} />
        <AppliedRoute path="/signup" exact component={Signup} props={childProps} />
        <AppliedRoute path="/poi/create" exact component={PointOfInterestAdd} props={childProps} />

        {/* ADMIN pages area */}
        <AppliedRoute path="/admin/poi/list" exact component={PoiList} props={childProps} />
        <AppliedRoute path="/admin/poi/detail" exact component={PoiDetail} props={childProps} />
        <AppliedRoute path="/admin/poi/edit" exact component={PointOfInterestEdit} props={childProps} />

        <AppliedRoute path="/admin/user/list" exact component={UserList} props={childProps} />
        <AppliedRoute path="/admin/user/detail" exact component={UserDetail} props={childProps} />
        <AppliedRoute path="/admin/user/edit" exact component={UserEdit} props={childProps} />

        {/* MANAGER pages area */}
        <AppliedRoute path="/place/list" exact component={ManagerPointList} props={childProps} />
        <AppliedRoute path="/place/detail" exact component={PlaceDetail} props={childProps} />
        <AppliedRoute path="/request/list" exact component={ManagerRequestList} props={childProps} />
        <AppliedRoute path="/request/edit" exact component={RequestEdit} props={childProps} />

        { /* Finally, catch all unmatched routes */ }
        <Route component={NotFound} />
    </Switch>;