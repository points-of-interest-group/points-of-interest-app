export const GOOGLE_MAP_API_URL = 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDxQZDjDZCmU0fr-o0VEgcF0_4ZdoTKFEk';

export const GOOGLE_PLACES_API_URL = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?key=AIzaSyBjh5wsXBgbn8u1qKmEhzS3qTixB_wfVP0&inputtype=textquery&language=ru&fields=name,geometry/location,formatted_address';
export const CORS_PROXY = 'https://cors-anywhere.herokuapp.com/';

export const API_BASE_URL = 'http://localhost:8080/api';
export const API_ADMIN_URL = 'http://localhost:8080/api/admin';
export const ACCESS_TOKEN = 'accessToken';