import React, {Component} from "react";
import {InfoWindow, Marker} from "react-google-maps";
import {Col, Form, FormControl, FormGroup, Grid, Row} from "react-bootstrap";
import './PointOfInterestMarker.css';
import {CurrentUserContext} from "../../util/CurrentUserContext";
import {getMarkerById, ratePointOfInterest} from "../../util/APIUtils";
import Loader from "../util/Loader";

export default class PointsOfInterestMarker extends Component {
    static contextType = CurrentUserContext;
    constructor(props) {
        super(props);

        this.state = {
            feature: null,
            infoWindowOpen: false,
            isLoading: true,
            isRate: false
        };

        this.handleMarkerClick = this.handleMarkerClick.bind();
    }
    componentDidMount() {
        this.loadMarkerInfo();
    }

    loadMarkerInfo() {
        this.setState({
            isLoading: true
        });
        getMarkerById(this.props.featureId)
            .then(response => {
                this.setState({
                    feature: response,
                    isLoading: false
                })
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isLoading: false
                })
            })
    }

    handleChange = event => {
        this.setState({
            isRate: true
        });

        const rateRequest = {
            pointOfInterestId: this.props.featureId,
            rate: event.target.value
        };

        ratePointOfInterest(rateRequest)
            .then(() => {
                this.setState({
                    isRate: false
                });
                this.loadMarkerInfo();
            })
            .catch(error => {
                console.log(error);
                this.setState({
                    isRate: false
                })
            })
    };

    handleMarkerClick = () => {
        this.setState({
            infoWindowOpen: !this.state.infoWindowOpen
        });
    };

    renderInfo(feature) {
        const currentUser =  this.context;
        const isUserRole = currentUser.role === 'ROLE_USER';

        return(
            <Form>
                <Grid fluid className="Marker">
                    <Row>
                        <Col xs={5}>Name:</Col>
                        <Col xs={7}>{feature.properties.name}</Col>
                    </Row>
                    <Row>
                        <Col xs={5}>Description:</Col>
                        <Col xs={7}>{feature.properties.description}</Col>
                    </Row>
                    <Row>
                        <Col xs={5}>Average rate:</Col>
                        <Col xs={7}>{feature.properties.average === '0,00' ? 'Not yet rated.' : feature.properties.average}</Col>
                    </Row>
                    {isUserRole && feature.properties.userRate &&
                        <Row>
                            <Col xs={5}>Your rate:</Col>
                            <Col xs={7}>{feature.properties.userRate}</Col>
                        </Row>
                    }
                    {isUserRole && !feature.properties.userRate &&
                        <Row>
                            <Col xs={5}>Rate</Col>
                            <Col xs={7}>
                                <FormGroup bsSize="small">
                                    <FormControl id="rate" componentClass="select" placeholder="rate"
                                                 onChange={this.handleChange} bsSize="small">
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                    </FormControl>
                                </FormGroup>
                            </Col>
                        </Row>
                    }
                </Grid>
            </Form>
        )
    }

    render() {
        const statuses = 'WRA';
        const feature = this.state.feature;

        if (this.state.isLoading) return (<Loader/>);
        return (
            <Marker
                key={this.props.featureId}
                position={{
                    lat: feature.geometry.coordinates[0],
                    lng: feature.geometry.coordinates[1]
                }}
                label={statuses[feature.properties.status]}
                onClick={(event) => this.handleMarkerClick(event, this.props.featureIndex)}>
                {this.state.infoWindowOpen &&
                    <InfoWindow onCloseClick={this.handleMarkerClick}>
                        <div style={{width: '250px', height: '150px'}}>
                            {this.state.isRate ? <Loader/> :  this.renderInfo(feature)}
                        </div>
                    </InfoWindow>
                }
            </Marker>
        )
    }
}