import React, {Component} from "react";
import Autocomplete from "react-autocomplete";
import {performSearch} from "../../util/APIUtils";
import './AutocompleteSearch.css';

export default class AutocompleteSearch extends Component {
    constructor(props) {
        super(props);

        this.state = {
            searchItems: [],
            selectedItem: null,
            searchValue: ''
        }
    }

    onSelectValue = (val, item) => {
        this.setState({searchValue: val, selectedItem: item})
    };

    onSearchValueChange = (e) => {
        const value = e.target.value;
        this.setState({
            searchValue: value
        });

        performSearch(value)
            .then(response => {
                console.log(response.candidates);
                this.setState({
                    searchItems: response.candidates
                });
            })
            .catch(err => {
                console.log(err);
            })
    };

    onCreateButtonClick = (e) => {
        e.preventDefault();

        const poi = {
            latitude: this.state.selectedItem.geometry.location.lat,
            longitude: this.state.selectedItem.geometry.location.lng,
            name: this.state.selectedItem.name
        };

        console.log(poi);

        this.props.handleCreatePOI(poi);
    };

    render() {
        return (
            <div className="Autocomplete">
                <Autocomplete
                    getItemValue={(value) => value.name}
                    items={this.state.searchItems}
                    renderItem={(item, isHighlighted) =>
                        <div
                            key={item.name}
                            className="item"
                            style={{
                                background: isHighlighted ? 'lightgray' : 'white'
                            }}>
                            <span className="name">{item.name} </span>
                            <span className="address">{item.formatted_address}</span>
                        </div>
                    }
                    value={this.state.searchValue}
                    onChange={this.onSearchValueChange}
                    onSelect={this.onSelectValue}
                    menuStyle={
                        {
                            borderRadius: '3px',
                            boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                            background: 'rgba(255, 255, 255, 0.9)',
                            fontSize: '90%',
                            position: 'fixed',
                            overflow: 'auto',
                            maxHeight: '50%', // TODO: don't cheat, let it flow to the bottom
                            zIndex: '150'
                        }}
                />
                <button style={{marginLeft: '20px'}} onClick={this.onCreateButtonClick}>Create</button>
            </div>
        );
    }
}