import React, {Component, Fragment} from "react";
import {Link, withRouter} from "react-router-dom";
import {LinkContainer} from "react-router-bootstrap";
import {Nav, Navbar, NavItem} from "react-bootstrap";
import "./App.css";

import Routes from "./Routes";
import {getCurrentUser} from "./util/APIUtils";
import {ACCESS_TOKEN} from "./constants";
import {CurrentUserContext} from "./util/CurrentUserContext";

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentUser: null,
            isAuthenticated: false,
            isAuthenticating: true,
            latitude: '',
            longitude: '',
            name: ''
        }
    }

    componentDidMount() {
        this.loadCurrentUser();
    }

    loadCurrentUser() {
        this.setState({
            isAuthenticating: true
        });
        getCurrentUser()
            .then(response => {
                this.setState({
                    currentUser: response,
                    isAuthenticated: true,
                    isAuthenticating: false
                });
            })
            .catch(() => {
                this.setState({
                    isAuthenticating: false
                });

                this.props.history.push('/login');
            });
    }

    handleLogout = () => {
        localStorage.removeItem(ACCESS_TOKEN);

        this.setState({
            currentUser: null,
            isAuthenticated: false
        });

        this.props.history.push("/login");
    };

    handleLogin = (authenticated) => {
        if (authenticated) {
            getCurrentUser()
                .then(response => {
                    this.setState({
                        currentUser: response,
                        isAuthenticated: true
                    });
                });
        } else {
            this.setState({isAuthenticated: false});
        }

        this.props.history.push("/");
    };

    handleCreatePOI = (poi) => {
        this.setState({
            latitude: poi.latitude,
            longitude: poi.longitude,
            name: poi.name
        });

        this.props.history.push("/poi/create");
    };

    render() {
        const childProps = {
            isAuthenticated: this.state.isAuthenticated,
            handleLogin: this.handleLogin,
            handleCreatePOI: this.handleCreatePOI,
            latitude: this.state.latitude,
            longitude: this.state.longitude,
            name: this.state.name
        };

        const currentUser = this.state.currentUser;
        const isAdmin = currentUser !== null && currentUser.role === 'ROLE_ADMIN';
        const isManager = currentUser !== null && currentUser.role === 'ROLE_MANAGER';

        return (
            !this.state.isAuthenticating &&
            <CurrentUserContext.Provider value={currentUser}>
                <div className="App container">
                    <Navbar fluid collapseOnSelect>
                        <Navbar.Header>
                            <Navbar.Brand>
                                <Link to="/">Scratch</Link>
                            </Navbar.Brand>
                            <Navbar.Toggle/>
                        </Navbar.Header>
                        <Navbar.Collapse>
                            {this.state.isAuthenticated && isAdmin &&
                            <Nav pullLeft>
                                <LinkContainer to="/admin/poi/list">
                                    <NavItem>Places</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/admin/user/list">
                                    <NavItem>Users</NavItem>
                                </LinkContainer>
                            </Nav>
                            }
                            {this.state.isAuthenticated && isManager &&
                            <Nav pullLeft>
                                <LinkContainer to="/place/list">
                                    <NavItem>My Places</NavItem>
                                </LinkContainer>
                                <LinkContainer to="/request/list">
                                    <NavItem>My Requests</NavItem>
                                </LinkContainer>
                            </Nav>
                            }
                            <Nav pullRight>
                                {this.state.isAuthenticated
                                    ? <Fragment>
                                        <Navbar.Text>Hello, {currentUser.name}</Navbar.Text>
                                        <NavItem onClick={this.handleLogout}>Logout</NavItem>
                                    </Fragment>
                                    : <Fragment>
                                        <LinkContainer to="/signup">
                                            <NavItem>Signup</NavItem>
                                        </LinkContainer>
                                        <LinkContainer to="/login">
                                            <NavItem>Login</NavItem>
                                        </LinkContainer>
                                    </Fragment>
                                }
                            </Nav>
                        </Navbar.Collapse>
                    </Navbar>
                    <Routes childProps={childProps}/>
                </div>
            </CurrentUserContext.Provider>
        );
    }
}

export default withRouter(App);